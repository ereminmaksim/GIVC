# REGISTER OF OBJECTS OF INTANGIBLE CULTURAL HERITAGE OF THE PEOPLES OF RUSSIA


## 🔴 <a href="https://givc.vercel.app/" target="_blank">Demonstration</a>

## Description
#### Click the link above to view developer versions of the apps.
## to run an internal command:
## -yarn i
## -yarn start

#### Data to create:
## .env.local: REACT_APP_API_URL=https://63300cde591935f3c888f9d4.mockapi.io

#### What was used:
- Sign up for a React account <a href='https://reactjs.org/'>go</a>
- Install Node JS in your computer <a href='https://nodejs.org/en/'>go</a>
- Styled-components react <a href='https://styled-components.com/'>go</a>
- React Content Loader <a href='https://skeletonreact.com/'>go</a>
- ContextApi <a href='https://reactjs.org/docs/context.html'>go</a>
- Typescript <a href='https://www.typescriptlang.org/'>go</a>
- Lodash <a href='https://lodash.com/'>go</a>
- Prettier <a href='https://prettier.io/'>go</a>
