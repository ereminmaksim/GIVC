import { instance } from 'api//getItems'
import { IItems } from 'interface/interface'
import React, { useState } from 'react'

interface ISearchContext {
	items: IItems[]
	setItems: React.Dispatch<React.SetStateAction<IItems[]>>

	isLoading: boolean
	setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
	setParams: React.Dispatch<React.SetStateAction<IParams>>
	getItems: () => Promise<void>
}
interface HeaderProps {
	children: React.ReactNode
}

interface IParams {
	search?: string
	categoryValue?: number
}

const Context = React.createContext<ISearchContext>({} as ISearchContext)

const ContentItemsContext = ({ children }: HeaderProps) => {
	const [items, setItems] = useState<IItems[]>([])
	const [isLoading, setIsLoading] = useState<boolean>(true)
	const [params, setParams] = useState<IParams>({
		search: undefined,
		categoryValue: undefined,
	})

	const getItems = async () => {
		try {
			const queryParams = `${params.search ? `&search=${params.search}` : ''}${
				params.categoryValue ? `&category=${params.categoryValue}` : ''
			}`

			await instance(queryParams)
				.then(res => res.json())
				.then(data => {
					setItems(data)
					setIsLoading(false)
				})
		} catch (err) {
			console.log(err)
		}
	}

	return (
		<Context.Provider
			value={{ getItems, setParams, items, setItems, isLoading, setIsLoading }}
		>
			{children}
		</Context.Provider>
	)
}
export default ContentItemsContext

export const useContentItemsContext = () => React.useContext(Context)
