import { ISort } from 'interface/interface'
import React, { useState } from 'react'

interface ISearchContext {
	sort: ISort

	setSort: React.Dispatch<React.SetStateAction<ISort>>
}
interface HeaderProps {
	children: React.ReactNode
}

const Context = React.createContext<ISearchContext>({} as ISearchContext)

const SortContext = ({ children }: HeaderProps) => {
	const [sort, setSort] = useState<ISort>({ name: 'Все регионы', sortProperty: 'all' })

	return <Context.Provider value={{ sort, setSort }}>{children}</Context.Provider>
}
export default SortContext

export const useSortContext = () => React.useContext(Context)
