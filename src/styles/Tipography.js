import { createGlobalStyle } from 'styled-components'

import HeuristicaBold from '../assets/fonts/Heuristica/Heuristicabold.ttf'
import HeuristicaItalic from '../assets/fonts/Heuristica/Heuristicaitalic.ttf'
import HeuristicaRegular from '../assets/fonts/Heuristica/Heuristicaregular.ttf'
import Roboto from '../assets/fonts/Roboto/Roboto.ttf'

const Typography = createGlobalStyle`
  @font-face {
    font-family: 'Heuristica Italic';
    src: url(${HeuristicaItalic});
    font-style: normal;
  }

  @font-face {
    font-family: 'Heuristica Regular';
    src: url(${HeuristicaRegular});
    font-style: normal;
  }

  @font-face {
    font-family: 'Heuristica Bold';
    src: url(${HeuristicaBold});
    font-style: normal;
  }

  @font-face {
    font-family: 'Roboto';
    src: url(${Roboto});
    font-style: normal;
  }


  html {
    font-family: 'Roboto', sans-serif;
    color: var(--gray-1);
  }
  
  body{
    background: #F5F5F5;
  }

  * {
    font-family: 'Roboto', sans-serif;
  }

  h1, h2, h3, h4, h5, h6 {
    font-family: 'Heuristica Regular', sans-serif;
  }

  p {
    color: black;
  }

  a {
    text-decoration: none;
    color: black;
  }

  li {
    list-style-type: none;
    color: black;
  }

  .content {
    background: #ebf3ff;
    padding: 0 20px 0 20px;
  }

  input:focus, textarea:focus, select:focus {
    outline: none !important;
    border: none;
  }
`
export default Typography
