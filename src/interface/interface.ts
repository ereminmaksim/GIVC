export interface IItems {
	id: string
	imgUrl: string
	title: string
	location: string
	resources?: string[]
}
export interface ISort {
	name: string
	sortProperty: string
}
