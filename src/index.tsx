import App from 'components/app/App'
import ContentItemsContext from 'context/contentItems/ContentItemsContext'
import SortContext from 'context/sort/SortContext'
import React from 'react'
import { createRoot } from 'react-dom/client'

import Typography from './styles/Tipography'

const root = createRoot(document.getElementById('root') as HTMLElement)

root.render(
	<React.StrictMode>
		<ContentItemsContext>
			<SortContext>
				<Typography />
				<App />
			</SortContext>
		</ContentItemsContext>
	</React.StrictMode>
)
