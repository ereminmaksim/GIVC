export function instance(search: string) {
	return fetch(`${process.env.REACT_APP_API_URL}/registry?${search}`)
}
