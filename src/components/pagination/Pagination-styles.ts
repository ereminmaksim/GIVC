import styled from 'styled-components'

export const StyledPaginateContainer = styled.div`
	margin-top: 53px;
	display: flex;
	justify-content: center;
	.pagination {
		li {
			display: inline-block;
			a {
				text-align: center;
				width: 35px;
				line-height: 36px;
				height: 35px;
				border-radius: 50%;
				margin-right: 20px;
				cursor: pointer;
				display: inline-block;
				color: #8791aa;
				font-size: 20px;
				background: rgba(25, 25, 25, 0.08);
				border: none;

				&:hover {
					color: white;
					background-color: #282828;
				}
			}
		}
		.selected {
			width: 35px;
			height: 35px;
			margin-right: 20px;

			a {
				color: #264576;
				background-color: white;
			}
		}
	}
`
