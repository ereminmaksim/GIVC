import ContentItems from 'components/contentItems/ContentItems'
import { useContentItemsContext } from 'context/contentItems/ContentItemsContext'
import { IItems } from 'interface/interface'
import React, { useEffect, useState } from 'react'
import ReactPaginate from 'react-paginate'

import { StyledPaginateContainer } from './Pagination-styles'

function Pagination() {
	const { items } = useContentItemsContext()

	const [currentItems, setCurrentItems] = useState<IItems[]>([])
	const [pageCount, setPageCount] = useState(0)
	const [itemOffset, setItemOffset] = useState(0)
	const itemsPerPage = 6

	useEffect(() => {
		const endOffset = itemOffset + itemsPerPage
		console.log(`Loading items from ${itemOffset} to ${endOffset}`)
		setCurrentItems(items?.slice(itemOffset, endOffset))
		setPageCount(Math?.ceil(items?.length / itemsPerPage))
	}, [itemOffset, itemsPerPage, items])

	const handlePageClick = (selectedItem: { selected: number }) => {
		const newOffset = (selectedItem.selected * itemsPerPage) % items.length
		setItemOffset(newOffset)
	}

	return (
		<>
			<ContentItems items={currentItems} />
			<StyledPaginateContainer>
				<ReactPaginate
					className="pagination"
					breakLabel="..."
					nextLabel="&#129082;"
					onPageChange={handlePageClick}
					pageRangeDisplayed={3}
					pageCount={pageCount}
					previousLabel="&#129080;"
					// @ts-ignore
					renderOnZeroPageCount={null}
				/>
			</StyledPaginateContainer>
		</>
	)
}

export default Pagination
