import glass from 'assets/images/glass.svg'
import debounce from 'lodash.debounce'
import React, { ChangeEvent, useCallback, useRef, useState } from 'react'
import { GrFormClose } from 'react-icons/gr'

import { WrapperTitleBlock } from './GlobalSearch-styles'

interface ITitleCompany {
	setSearchValue: (text: string) => void
}

const GlobalSearch = ({ setSearchValue }: ITitleCompany) => {
	const [value, setValue] = useState('')
	const inputRef = useRef<HTMLInputElement | any>(null)

	const handleSearchClear = () => {
		setSearchValue('')
		setValue('')
		if (inputRef) {
			inputRef.current.focus()
		}
	}

	const handleUpdateSearchValue = useCallback(
		debounce((str: string) => {
			setSearchValue(str)
		}, 1000),
		[]
	)

	const handleSearchValue = (e: ChangeEvent<HTMLInputElement>) => {
		setValue(e.currentTarget.value)
		handleUpdateSearchValue(e.currentTarget.value)
	}

	return (
		<WrapperTitleBlock>
			<h1 className="left">
				Реестр объектов нематериального культурного наследия народов россии
			</h1>
			<div className="blockRight">
				<input
					ref={inputRef}
					value={value}
					onChange={handleSearchValue}
					type="text"
					placeholder="Поиск..."
				/>
				{value ? (
					<GrFormClose onClick={handleSearchClear} className="close" />
				) : (
					<img className="glassImg" src={glass} alt="glass" />
				)}
			</div>
		</WrapperTitleBlock>
	)
}
export default GlobalSearch
