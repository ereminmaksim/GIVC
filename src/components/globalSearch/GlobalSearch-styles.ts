import styled from 'styled-components'

export const WrapperTitleBlock = styled.div`
	padding-top: 40px;
	display: flex;
	justify-content: space-between;
	align-items: center;
	@media only screen and (max-width: 650px) {
		flex-wrap: wrap;
	}

	.left {
		font-weight: 400;
		font-size: 20px;
		line-height: 24px;
		text-transform: uppercase;
		color: #264576;
		width: 556px;
		height: 48px;
		@media only screen and (max-width: 718px) {
			font-size: 14px;
		}
	}

	.blockRight {
		position: relative;

		input {
			padding-left: 16px;
			width: 400px;
			height: 40px;
			background: #ffffff;
			box-shadow: 0 4px 7px rgba(100, 177, 252, 0.3);
			border-radius: 20px;
			border: none;
			font-weight: 400;
			font-size: 16px;
			line-height: 19px;
			color: #8791aa;
		}

		.close {
			position: absolute;
			right: 19px;
			bottom: 11px;
			height: 20px;
			color: #b38787;
			cursor: pointer;

			:hover {
				color: #282828;
			}
		}

		.glassImg {
			position: absolute;
			right: 19px;
			top: 12px;
			background: white;
			cursor: pointer;
		}
	}
`
