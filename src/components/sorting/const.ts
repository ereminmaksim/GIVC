export const sortRegions = [
	{ _id: 0, name: 'Все регионы', sortProperty: 'all' },
	{ _id: 1, name: 'Смоленская область', sortProperty: 'smolensk' },
	{ _id: 2, name: 'Вологодская область', sortProperty: 'vologda' },
	{ _id: 3, name: 'Сaхалинская область', sortProperty: 'sakhalin' },
]
