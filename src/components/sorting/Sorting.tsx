import wayDown from 'assets/images/way_down.svg'
import { sortRegions } from 'components/sorting/const'
import { useContentItemsContext } from 'context/contentItems/ContentItemsContext'
import { useSortContext } from 'context/sort/SortContext'
import { useOutside } from 'hooks/useOutside'
import React from 'react'

import { Sort } from './Sorting-styles'

interface ISortRegions {
	_id: number
	name: string
	sortProperty: string
}

const Sorting = () => {
	const { setParams } = useContentItemsContext()
	const { sort, setSort } = useSortContext()
	const { ref, isShow, setIsShow } = useOutside(false)

	const onPopUp = (obj: ISortRegions) => {
		setSort(obj)
		setIsShow(false)
		setParams(prev => {
			const newSort = prev
			newSort.categoryValue = obj._id
			return newSort
		})
	}

	return (
		<Sort>
			<div className="sort__label">
				<img className="downImg" src={wayDown} alt="wayDown" />
				<span onClick={() => setIsShow(!isShow)}>{sort.name}</span>
			</div>

			{isShow && (
				<div className="sort__popup" ref={ref}>
					<ul>
						{sortRegions.map((region, i) => (
							<li
								key={i}
								onClick={() => onPopUp(region)}
								className={sort.sortProperty === region.sortProperty ? 'active' : ''}
							>
								{region.name}
							</li>
						))}
					</ul>
				</div>
			)}
		</Sort>
	)
}
export default React.memo(Sorting)
