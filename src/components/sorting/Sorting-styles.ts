import styled from 'styled-components'

export const Sort = styled.div`
	.sort {
		position: relative;

		&__label {
			display: flex;
			align-items: center;
			position: relative;
			width: 340px;
			height: 35px;
			justify-content: center;
			border: none;
			background: #ffffff;
			box-shadow: 0 4px 7px rgba(100, 177, 252, 0.3);
			border-radius: 20px;
			cursor: pointer;
			font-size: 16px;
			line-height: 19px;
			color: #8791aa;

			.downImg {
				position: absolute;
				right: 16px;
				cursor: pointer;
			}

			@media only screen and (max-width: 735px) {
				margin-top: 5px;
			}

			svg {
				margin-right: 8px;
			}

			b {
				margin-right: 8px;
			}

			span {
				color: gray;
				cursor: pointer;
			}
		}

		&__popup {
			position: absolute;
			right: 35px;
			background: #ffffff;
			box-shadow: 0 5px 15px rgba(0, 0, 0, 0.09);
			border-radius: 10px;
			overflow: hidden;
			padding: 10px;
			z-index: 1;

			ul {
				overflow: hidden;
				margin: 0;
				padding: 0;

				li {
					padding: 12px 20px;
					cursor: pointer;

					&.active,
					&:hover {
						background: rgba(21, 47, 215, 0.05);
					}

					&.active {
						font-weight: bold;
						color: green;
					}
				}
			}
		}
	}
`
