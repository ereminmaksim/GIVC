import { useContentItemsContext } from 'context/contentItems/ContentItemsContext'
import React from 'react'

import { WrapperDataContent } from './DataContent-styles'

const DataContent = () => {
	const { items } = useContentItemsContext()
	return (
		<WrapperDataContent>
			<span>Всего объектов: {items.length}</span>
		</WrapperDataContent>
	)
}
export default DataContent
