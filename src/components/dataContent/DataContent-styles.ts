import styled from 'styled-components'

export const WrapperDataContent = styled.span`
	background: #ebf3ff;
	> span {
		display: inline-block;
		margin-top: 40px;
		font-weight: 400;
		font-size: 14px;
		line-height: 16px;
		color: rgba(25, 25, 25, 0.5);
		background: #ebf3ff;
	}
`
