import { navigationList } from 'components/header/const'
import React from 'react'

import { WrappersHeader } from './Header-styles'

const Header = () => {
	return (
		<WrappersHeader>
			{navigationList.map((navigation, index) => (
				<ul key={index}>
					<li>{navigation}</li>
				</ul>
			))}
		</WrappersHeader>
	)
}
export default Header
