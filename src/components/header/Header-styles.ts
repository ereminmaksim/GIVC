import styled from 'styled-components'

export const WrappersHeader = styled.div`
	border-radius: 20px 20px 0 0;
	display: flex;
	justify-content: start;
	align-items: center;
	height: 30px;
	background: #ffffff;
	width: 100%;
	padding: 0 20px 0 20px;
	> span {
		margin-left: 20px;
		font-weight: 400;
		font-size: 14px;
		line-height: 16px;
		color: #264576;
		background: white;
		cursor: pointer;
	}
`
