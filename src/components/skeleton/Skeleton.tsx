import React from 'react'
import ContentLoader from 'react-content-loader'

const Skeleton = () => (
	<ContentLoader
		speed={3}
		width={355}
		height={364}
		viewBox="0 0 355 364"
		backgroundColor="#e3eaf2"
		foregroundColor="#a8c8e6"
	>
		<rect x="8" y="42" rx="0" ry="0" width="355" height="364" />
	</ContentLoader>
)

export default Skeleton
