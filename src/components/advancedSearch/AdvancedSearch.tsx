import SearchBlock from 'components/advancedSearch/searchBlock/SearchBlock'
import TitleBlock from 'components/advancedSearch/titleBlock/TitleBlock'
import React from 'react'

import { WrapperAdvancedSearch } from './AdvancedSearch-styles'

const AdvancedSearch = () => {
	return (
		<WrapperAdvancedSearch>
			<TitleBlock />
			<SearchBlock />
		</WrapperAdvancedSearch>
	)
}
export default AdvancedSearch
