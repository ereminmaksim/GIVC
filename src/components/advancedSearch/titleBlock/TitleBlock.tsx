import glass from 'assets/images/glass.svg'
import React from 'react'

import { WrapperTitleBlock } from './TitleBlock-styles'

const TitleBlock = () => {
	return (
		<WrapperTitleBlock>
			<div className="leftBlock">
				<span>Главная</span>
				<h2>Расширенный поиск</h2>
			</div>
			<div className="rightBlock">
				<input type="text" placeholder="Контекстный поиск" />
				<img className="glassImg" src={glass} alt="glass" />
			</div>
		</WrapperTitleBlock>
	)
}
export default TitleBlock
