import styled from 'styled-components'

export const WrapperTitleBlock = styled.div`
	border-radius: 16px 16px 0 0;
	display: flex;
	justify-content: space-between;
	align-items: center;

	.leftBlock {
		> span {
			font-weight: 400;
			font-size: 14px;
			line-height: 16px;
			color: rgba(25, 25, 25, 0.5);
		}

		> h2 {
			font-weight: 400;
			font-size: 20px;
			line-height: 24px;
			text-transform: uppercase;
			color: #264576;
		}
	}

	.rightBlock {
		position: relative;
		background: none;

		> input {
			padding: 8px 16px;
			width: 221px;
			height: 40px;
			background: #f5f6f7 !important;
			border: none;
			border-radius: 100px;
			outline: none;
			font-weight: 400;
			font-size: 16px;
			line-height: 19px;
			color: #8791aa;
		}

		.glassImg {
			position: absolute;
			top: 19px;
			right: 11px;
		}
	}
`
