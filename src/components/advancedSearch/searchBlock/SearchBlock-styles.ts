import styled from 'styled-components'

export const WrapperSearchBlock = styled.div`
	.searchBtn {
		margin-top: 30px;
		position: absolute;
		right: 0;
		margin-right: 20px;
		padding: 16px 80px;
		width: 207px;
		height: 51px;
		background: #1e82fa;
		border-radius: 100px;
		outline: none;
		font-weight: 500;
		font-size: 16px;
		line-height: 19px;
		color: #ffffff;
		cursor: pointer;
		border: none;
		-webkit-transition: all 250ms cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;
		transition: all 250ms cubic-bezier(0.25, 0.46, 0.45, 0.94) 0s;

		:hover {
			box-shadow: rgb(0 0 0 / 13%) 0 40px 58px -16px, rgb(0 0 0 / 31%) 0 30px 22px -17px;
			border-color: rgba(74, 174, 169, 0.8);
		}

		@media only screen and (max-width: 1075px) {
			margin-top: 0;
		}
		@media only screen and (max-width: 752px) {
			margin-left: 10px;
			left: 36%;
		}
		@media only screen and (max-width: 735px) {
			left: 36%;
		}
	}
`
export const WrapperBlock = styled.div`
	display: flex;
	width: 100%;
	justify-content: space-around;
	flex-wrap: wrap;

	.block {
		position: relative;
		width: 340px;
		height: 35px;
		display: flex;
		justify-content: center;
		align-items: center;
		border: none;
		background: #ffffff;
		box-shadow: 0 4px 7px rgba(100, 177, 252, 0.3);
		border-radius: 20px;
		cursor: pointer;
		font-size: 16px;
		line-height: 19px;
		color: #8791aa;
		margin-bottom: 16px;

		.downImg {
			position: absolute;
			right: 16px;
			cursor: pointer;
		}

		@media only screen and (max-width: 735px) {
			margin-top: 5px;
		}
	}
`
