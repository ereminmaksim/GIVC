import wayDown from 'assets/images/way_down.svg'
import { fields } from 'components/advancedSearch/searchBlock/const'
import Sorting from 'components/sorting/Sorting'
import { useContentItemsContext } from 'context/contentItems/ContentItemsContext'
import React from 'react'

import { WrapperBlock, WrapperSearchBlock } from './SearchBlock-styles'

const SearchBlock = () => {
	const { getItems } = useContentItemsContext()

	const filters = (
		<>
			{fields.map((field, index) => (
				<button key={index} className="block">
					{field}
					<img className="downImg" src={wayDown} alt="wayDown" />
				</button>
			))}
		</>
	)

	return (
		<WrapperSearchBlock>
			<WrapperBlock>
				{filters}
				<Sorting />
			</WrapperBlock>
			<button onClick={getItems} className="searchBtn">
				Поиск
			</button>
		</WrapperSearchBlock>
	)
}
export default SearchBlock
