import styled from 'styled-components'

export const WrapperAdvancedSearch = styled.div`
	padding: 20px 20px 0 20px;
	min-height: 285px;
	margin-top: 50px;
	border-radius: 16px;
	background: #ffffff;
	position: relative;

	@media only screen and (max-width: 1075px) {
		min-height: 300px;
	}
	@media only screen and (max-width: 752px) {
		min-height: 475px;
	}
	@media only screen and (max-width: 735px) {
		min-height: 475px;
	}
`
