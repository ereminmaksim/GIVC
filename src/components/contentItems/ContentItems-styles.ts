import styled from 'styled-components'

export const WrapContent = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: start;
	background: #ebf3ff;

	@media only screen and (max-width: 1113px) {
		justify-content: space-around;
	}
`

export const WrapperContentItems = styled.div`
	margin-top: 40px;
	width: 355px;
	height: 364px;
	border-radius: 16px;
	background: #ffff;
	margin-right: 11px;

	.wrapItems {
		display: flex;
		flex-direction: column;
	}

	.plugImg {
		background: #ffff;
		width: 356px;
		height: 211px;
		border-radius: 16px 16px 0 0;
		object-fit: cover;
	}

	.wrapContentText {
		padding-left: 20px;
		background: #ffff;

		.description {
			margin-top: 26px;
			background: #ffff;

			> span {
				font-family: 'Roboto', sans-serif !important;
				font-weight: 900;
				font-size: 16px;
				line-height: 19px;
				color: #000000;
				text-align: center;
				background: #ffff;
			}
		}

		.location {
			margin-top: 16px;
			display: flex;
			align-items: center;
			margin-right: 7px;
			background: #ffff;

			> span {
				font-weight: 400;
				font-size: 14px;
				line-height: 16px;
				color: #191919;
				background: #ffff;
			}
		}

		.actions {
			margin-top: 20px;
			display: flex;
			justify-content: start;
			margin-right: 17px;

			.resource {
				cursor: pointer;
			}
		}
	}
`
