import location from 'assets/images/location.svg'
import Skeleton from 'components/skeleton/Skeleton'
import { useContentItemsContext } from 'context/contentItems/ContentItemsContext'
import { IItems } from 'interface/interface'
import React, { useMemo } from 'react'

import { WrapContent, WrapperContentItems } from './ContentItems-styles'

export interface IContentItems {
	items: IItems[]
}

const ContentItems = ({ items }: IContentItems) => {
	const { isLoading } = useContentItemsContext()

	const skeleton = useMemo(
		() => [...new Array(6)].map((_, index) => <Skeleton key={index} />),
		[]
	)

	const contentItems = useMemo(
		() =>
			items?.map(item => (
				<WrapperContentItems key={item.id}>
					<img className="plugImg" src={item.imgUrl} alt="heritage" />
					<div className="wrapContentText">
						<div className="description">
							<span>{item.title}</span>
						</div>
						<div className="location">
							<img src={location} alt="location" />

							<span>{item.location}</span>
						</div>
						<div className="actions">
							{item.resources?.map(photo => (
								<img key={photo} className="resource" src={photo} alt="photos" />
							))}
						</div>
					</div>
				</WrapperContentItems>
			)),
		[items]
	)
	return <WrapContent>{isLoading ? skeleton : contentItems}</WrapContent>
}
export default React.memo(ContentItems)
