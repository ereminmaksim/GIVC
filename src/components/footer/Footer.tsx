import castle from 'assets/images/castle.png'
import committee from 'assets/images/committee.png'
import React from 'react'

import { WrapperFooter } from './Footer-styles'

const Footer = () => {
	return (
		<>
			<WrapperFooter>
				<div className="wrapLeftBlock">
					<img src={castle} alt="castle" />
					<span>Государственный Российский Дом народного творчества имени В.Д. Поленова</span>
				</div>
				<div className="wrapCenterBlock">
					<p className="contacts">Контакты</p>
					<p className="address">Москва, Сверчков пер., 8, стр. 3.</p>
					<p className="phone">Тел.: +7 495 628-40-87</p>
				</div>
				<div className="wrapRightBlock">
					<img src={committee} alt="committee" />
				</div>
			</WrapperFooter>
			<br />
		</>
	)
}
export default Footer
