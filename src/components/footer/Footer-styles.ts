import styled from 'styled-components'

export const WrapperFooter = styled.div`
	margin-top: 100px;
	display: flex;
	justify-content: space-between;
	align-items: center;
	margin-bottom: 50px;
	@media only screen and (max-width: 1112px) {
		justify-content: space-evenly;
	}

	.wrapLeftBlock {
		display: flex;
		align-items: center;

		img {
			margin-right: 25px;

			@media only screen and (max-width: 1112px) {
				width: 120px;
				margin-right: 10px;
			}
		}

		span {
			font-family: 'Heuristica', sans-serif;
			width: 300px;
			font-weight: 400;
			font-size: 16px;
			line-height: 19px;
			text-transform: uppercase;
			color: #264576;

			@media only screen and (max-width: 1112px) {
				font-size: 11px;
			}
		}
	}

	.wrapCenterBlock {
		.contacts {
			font-family: 'Heuristica', sans-serif;
			font-weight: 400;
			font-size: 16px;
			line-height: 19px;
			text-transform: uppercase;
			color: #264576;
			@media only screen and (max-width: 1112px) {
				font-size: 11px;
			}
		}

		.phone {
			font-family: 'Heuristica', sans-serif;
			font-weight: 400;
			font-size: 14px;
			line-height: 17px;
			text-transform: uppercase;
			color: #264576;
			@media only screen and (max-width: 1112px) {
				font-size: 11px;
			}
		}

		.address {
			font-family: 'Heuristica', sans-serif;
			font-weight: 400;
			font-size: 14px;
			line-height: 17px;
			text-transform: uppercase;
			color: #264576;
			height: 0;
			@media only screen and (max-width: 1112px) {
				font-size: 11px;
			}
		}
	}

	.wrapRightBlock {
		img {
			@media only screen and (max-width: 916px) {
				display: none;
			}
		}
	}
`
