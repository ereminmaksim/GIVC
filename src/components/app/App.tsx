import AdvancedSearch from 'components/advancedSearch/AdvancedSearch'
import DataContent from 'components/dataContent/DataContent'
import Footer from 'components/footer/Footer'
import GlobalSearch from 'components/globalSearch/GlobalSearch'
import Header from 'components/header/Header'
import Pagination from 'components/pagination/Pagination'
import { useContentItemsContext } from 'context/contentItems/ContentItemsContext'
import React, { useCallback, useEffect } from 'react'

import { Main, MainWrapper } from './App-styles'

const App = () => {
	const { getItems, setParams } = useContentItemsContext()

	const onSearchHandler = useCallback(
		(str: string) => {
			setParams(prev => {
				const newSearch = prev
				newSearch.search = str
				return newSearch
			})
			getItems().catch(console.log)
		},
		[getItems, setParams]
	)

	useEffect(() => {
		getItems().catch(console.log)
	}, [])

	return (
		<Main>
			<Header />
			<MainWrapper className="content">
				<GlobalSearch setSearchValue={onSearchHandler} />
				<AdvancedSearch />
				<DataContent />
				<Pagination />
				<Footer />
			</MainWrapper>
		</Main>
	)
}
export default App
