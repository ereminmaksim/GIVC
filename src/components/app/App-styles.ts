import styled from 'styled-components'

export const Main = styled.div`
	margin: 10px auto;
	border-radius: 10px;
	max-width: 1100px;
`

export const MainWrapper = styled.div`
	min-height: 100vh;
	width: 100%;
	border-radius: 0 0 20px 20px;
`
